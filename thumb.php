<?php
define('CL_ROOT', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR));

function getArrayVal(array $array, $name)
{
    if (array_key_exists($name, $array)) {
        return $array[$name];
    }
}

error_reporting(0);
$pic = getArrayVal($_GET, "pic");
$height = getArrayVal($_GET, "height");
$width = getArrayVal($_GET, "width");

include(CL_ROOT . "/include/class.hft_image.php");

$pic=CL_ROOT . "/" . $pic;

list($imagewidth, $imageheight)  = getimagesize($pic);

//$imagepath = $pic;
// header("Content-type: image/jpeg");
// //原图文件
// $file = $imagepath;
// // 尺寸
// list($width, $height) = getimagesize($file);
//
// if (!isset($height)) {
//     $ratio =  $imageheight / $imagewidth;
//     $height = $width * $ratio;
//     $height = round($height);
// }
//
// if (!isset($width)) {
//     $ratio = $imagewidth / $imageheight;
//     $width = $height * $ratio;
// }
//
// $newwidth = $width;
// $newheight = $height;
//
// // 加载图像（只支持jpg)
// $src_im = @imagecreatefromjpeg($file);
// $dst_im = imagecreatetruecolor($newwidth, $newheight);
//
// // 调整大小
// imagecopyresized($dst_im, $src_im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
//
// //输出缩小后的图像
// imagejpeg($dst_im);
//
// imagedestroy($dst_im);
// imagedestroy($src_im);
// die();

$myThumb = new hft_image($pic);
$myThumb->jpeg_quality = 80;


if (!isset($height)) {
    $ratio =  $imageheight / $imagewidth;
    $height = $width * $ratio;
    $height = round($height);

    if($height==0)
    {
        $height = $imagewidth;
    }
}

if (!isset($width)) {
    $ratio = $imagewidth / $imageheight;
    $width = $height * $ratio;
    $width = round($width);
}

$myThumb->resize($width, $height, 0);

HEADER("Content-Type: image/jpeg");
$myThumb->output_resized('');

?>