# p2_plan_team

#### 介绍
2-plan-team 最新版本修改支持php7,开始设计

#### 软件架构
软件架构说明
原始地址为 http://2-plan.com/download-project-management-software.html

本次修改方法

mysql 修改: class.datenbank.php
html编译模板更新: smarty-3.1.34


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1. 使用php运行
```
cc@cc-Macmini:/opt/web-php/2-plan-team$ php -S localhost:8888 
```
> 浏览器收入地址 http://localhost:8888/install.php 安装此软件


2.  nginx

```
cc@cc-Macmini:/opt/web-php/p2_plan_team$ sudo chown www-data templates_c

drwxr-xr-x  3 www-data cc  4096 12月 29 15:09 templates_c/

sudo chown www-data files/standard
#这个目录为上传用户照片目录

```
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

