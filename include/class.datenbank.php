<?php

/*
* The class datenbank (database) provides methods to handle a database connection
*
* @author original code from Open Dynamics.
* @name datenbank
* @version 0.4.6
* @package 2-plan
* @link http://2-plan.com
* @license http://opensource.org/licenses/gpl-license.php GNU General Public License v3 or later
*/
global $mysqli;
$mysqli = null;

class datenbank
{

    /*
     * Constructor
     */
    function __construct()
    {
    }

    /*
    * Establish a database connection
    *
    * @param string $db Database name
    * @param string $user Database user
    * @param string $pass Password for database access
    * @param string $host Database host
    * @return bool
    */
    function connect($db_name, $db_user, $db_pass, $db_host="localhost", $db_port=3306)
    {
        if (!function_exists('mysql_select_db')) {
            // function mysql_connect($dbhost, $dbuser, $dbpass)
            // {
            //     global $db_port;
            //     global $db_name;
            //     global $mysqli;
            //     $mysqli = mysqli_connect("$dbhost:$db_port", $dbuser, $dbpass, $db_name);
            //     return $mysqli;
            // }
            function mysql_fetch_object($result){
                
                return mysqli_fetch_object($result);
            }

            function mysql_select_db($dbname)
            {
                global $mysqli;
                return mysqli_select_db($mysqli, $dbname);
            }
            function mysql_fetch_array($result)
            {
                return mysqli_fetch_array($result);
            }
            function mysql_fetch_assoc($result)
            {
                return mysqli_fetch_assoc($result);
            }
            function mysql_fetch_row($result)
            {
                return mysqli_fetch_row($result);
            }
            function mysql_query($query)
            {
                global $mysqli;
                error_log($query."\r\n", 0);
                return mysqli_query($mysqli, $query);
            }
            function mysql_escape_string($data)
            {
                global $mysqli;
                return mysqli_real_escape_string($mysqli, $data);
            }
            function mysql_real_escape_string($data)
            {
                global $mysqli;
                return mysqli_real_escape_string($mysqli,$data);
            }
            function mysql_close()
            {
                global $mysqli;
                return mysqli_close($mysqli);
            }
            function mysql_error()
            {
                global $mysqli;
                return mysqli_connect_error();
            }
            function mysql_insert_id()
            {
                global $mysqli;
                return mysqli_insert_id($mysqli);
            }
            function mysql_num_rows($data)
            {
                global $mysqli;
                return mysqli_num_rows($data);
            }
            function mysql_affected_rows()
            {
                global $mysqli;
                return mysqli_affected_rows($mysqli);
            }
        }
        //mysql
        //$db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
        //return true;   
        global $mysqli;

        //$conn = mysql_connect($db_host, $db_user, $db_pass);
        $mysqli = mysqli_connect("$db_host:$db_port", $db_user, $db_pass, $db_name);
        $conn = $mysqli;
        //设定默认编码为utf8
        mysqli_set_charset($mysqli, "utf8");

        mysql_select_db($db_name);
        // 检查连接 
        if (!$conn) {
            die("连接错误: " . mysqli_connect_error());
            return false;
        }
        return true;

        // $db_check = mysql_select_db($db_name);
        // if($db_check)
        // {
        //     return true;
        // }
        // else
        // {
        //     return false;
        // }
    }

    /*
     * Wrap mysql_query function
     *
     * @param string $str SQL search query
     * @return bool
     */
    function query($str)
    {
    	return mysql_query($str);
    }
}
?>